![Logo](assets/arvutidark-logo.png)

### Stylus UserCSS/LESS tume stiil Arvutitark.ee veebilehele

![Screenshot](assets/screenshot.png)

## Paigaldamine
Esmalt [![Paigalda laiendus Stylus](https://img.shields.io/badge/Paigalda%20laiendus-Stylus-00adad)](https://add0n.com/stylus.html) ja seejärel [![Lisa stiil Stylus](https://img.shields.io/badge/Lisa%20stiil-Stylus-00adad)](https://gitlab.com/treierxyz/arvutidark/raw/main/arvutidark.user.css).
Stiil on saadaval ka UserStyles.world keskkonnas: [![Lisa stiil Userstyles](https://img.shields.io/badge/Lisa%20stiil-UserStyles.world-0066bb)](https://userstyles.world/style/7312/arvutidark).

Peale paigaldamist peaks teie Arvutitarga lehitsemine olema silmadele mugavam!

## Uuendamine
Tavaliselt uuendab Stylus stiile automaatselt, aga uuendamise sundimiseks avage Stylus oma lisanditest ja sealt valige stiilide haldur (**Manage**). Stiili kõrval on luubi kujuline nupp (<img src="assets/update.svg" width="28px">) millega saab sundida uuendamist.

## Panustamine
Ootan meeleldi täiendusi sellele stiilile, eriti on teretulnud koodi lihtsustavad merge requestid.

## Tänud
Tänud härra [![SEPIC!](https://img.shields.io/badge/SEPIC!-YouTube-ff0000)](https://youtube.com/sepicgamer) nime välja mõtlemise eest.

## Litsents
Projekt on MIT-litsentsi all. Kui see projekt saaks aluseks Arvutitarga ametlikule tumedale stiilile, siis oleks ma väga tänulik (kui see kunagi juhtub, võibolla teevad hoopis uue veebilehe ja see projekt vananeb), nii et valisin seda võimalust lubava litsentsi.
